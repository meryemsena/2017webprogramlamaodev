﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(webprojesi.Startup))]
namespace webprojesi
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
